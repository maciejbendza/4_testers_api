
import requests
class GoRESTHandler:

    base_url = "https://gorest.co.in/public/v2"
    users_endpoint = "/users"
    header={
        "Authorization": "Bearer 08a809d143a6695636208500545cef2029b5c246a38143fd743d763c5268e06e"}

    def create_user(self,user_data):
        response = requests.post(self.base_url + self.users_endpoint, json=user_data, headers=self.header)
        assert response.status_code == 201
        return response
    def get_user(self, user_id):
        response = requests.get(f"{self.base_url}{self.users_endpoint}/{user_id}", headers=self.header)
        assert response.status_code == 200
        return response
    def update_user(self, user_id,user_data):
        response = requests.put(f"{self.base_url}{self.users_endpoint}/{user_id}",json=user_data, headers=self.header)
        return response
    def delete_user(self,user_id):
        response = requests.delete(f"{self.base_url}{self.users_endpoint}/{user_id}", headers=self.header)
        assert response.status_code == 204
        return response
